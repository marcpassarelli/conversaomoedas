import {
View,Text,Picker,TouchableOpacity,ActivityIndicator
} from 'react-native';
import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {getUpdatedApiData,checkIfUpdated} from './src/API'
import ConversaoMoedas from './src/ConversaoMoedas';

export default class App extends Component{

  constructor(props){
    super(props);

    this.state = {
      loading:false,
      lastDate:'',
      atualizado:'',
      localRates:[],
      localRatesWValues:[],
      moeda1:'BRL',
      moeda2:'BRL',
      valorConvertido:1
    }

  }

  async componentDidMount(){

    this.getOfflineData()
  }

  //Função para buscar as informações salvas offline no dispositivo
  getOfflineData = async () => {
    try {
      this.setState({
        loading:true
      },function(){
      });
      var localRates = await AsyncStorage.getItem('rates')
      var localRatesWValues = JSON.parse(localRates)
      //verifica se há algo salvo offline
      if(localRates !== null) {
        //se houver algo salvo offline então atualizará os states de localDate e localRates para serem usados
        localRates = Object.keys(JSON.parse(localRates))
        this.setState({
          localDate: await AsyncStorage.getItem('date'),
          localRatesWValues: localRatesWValues,
          localRates: localRates.sort(),
        });

        //checa se as informações salvas offline são mais recentes
        checkIfUpdated(this.state.localDate,
          //se estiver atualizado
          ()=>{

            this.setState({
              atualizado: true,
              loading:false
            });
          },
          //se não estiver atualizado
          ()=>{
            this.setState({
              atualizado:false,
              loading:false
            });
          });

      }else{
        //caso não tenha nada salvo, buscará o mais recente
        getUpdatedApiData((date,rates)=>{
          this.setState({
            localDate: date,
            localRates: rates,
            atualizado: true,
            loading:false
          });
        })

      }
    } catch(e) {
      // error reading value
    }
  }

  //atualizar o valor da moeda com o valor selecionado no Picker1
  updateMoeda1 = (value) => {
     this.setState({ moeda1: value })
  }

  //atualizar o valor da moeda com o valor selecionado no Picker2
  updateMoeda2 = (value) => {
     this.setState({ moeda2: value })
  }

  //atualizar valor a ser convertido
  updateValor1 = (text) =>{
    this.setState({ valor1: text});
  }

  //função para converter valor 
  async converterValor(valor,moeda1,moeda2){
    try {
      this.setState({
        loading:true
      });

      let valorConvertido =  valor * (this.state.localRatesWValues[moeda2] / this.state.localRatesWValues[moeda1])
      this.setState({
        loading:false,
        valorConvertido:valorConvertido
      });

    } catch (e) {

    }

  }

  render(){
    const content = this.state.loading ?

    /*enquanto loading for true mostrará o ActivityIndicator para o usuário
      indicando que há informações sendo carregadas*/
    <View style={{ flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'}}>
      <ActivityIndicator size="large" color={"black"} />
    </View> :

    <ConversaoMoedas
      getUpdatedApiData={()=>{
        getUpdatedApiData((date,rates)=>{
          this.setState({
            localDate: date,
            localRates: rates,
            atualizado: true,
            loading:false
          });
        })
      }}
      localRates={this.state.localRates}
      atualizado={this.state.atualizado}
      localDate={this.state.localDate}
      moeda1={this.state.moeda1}
      updateMoeda1={this.updateMoeda1}
      moeda2={this.state.moeda2}
      updateMoeda2={this.updateMoeda2}
      valor1={this.state.valor1}
      updateValor1={this.updateValor1}
      converterValor={()=>{this.converterValor(this.state.valor1,this.state.moeda1,this.state.moeda2)}}
      valorConvertido={this.state.valorConvertido}
      />

    return(
      <View style={{flex:1}}>
        {content}
      </View>
    )
  }
}
