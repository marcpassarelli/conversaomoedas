import { Text, View, TextInput, TouchableOpacity,Picker,Button  } from 'react-native';
import React, { Component } from 'react';

export default ConversaoMoedas = (props) => {
  return (
    <View style={{alignContent: 'center'}}>
      <Text style={{alignSelf: 'center',marginTop: 10}}>Informe o valor a ser convertido</Text>
      <TextInput
        underlineColorAndroid='transparent'
        value={props.valor1}
        style={{marginBottom: 20,
          borderWidth: 1,
          fontSize: 16,
          height: 40,
          paddingLeft: 15,
          width: 100,
          alignSelf: 'center',
        alignContent: 'center'}}

        keyboardType={'numeric'}
        onChangeText={props.updateValor1}/>
      <Picker
        style={{marginLeft: 30,height: 50,justifyContent: 'center',alignSelf: 'center',width: 120}}
        selectedValue={props.moeda1}
        onValueChange={props.updateMoeda1}>
        {props.localRates.map((item, index)=>{
          return (<Picker.Item label={item}
            value={item} key={index} />)
        })}
      </Picker>
      <Picker
        style={{marginLeft: 30,height: 50,justifyContent: 'center',alignSelf: 'center',alignContent: 'center',width: 120,marginTop: 30}}
        selectedValue={props.moeda2}
        onValueChange={props.updateMoeda2}>
        {props.localRates.map((item, index)=>{
          return (<Picker.Item label={item}
            value={item} key={index} />)
        })}
      </Picker>
      <Text style={{alignSelf: 'center',marginBottom: 10}}>{props.valorConvertido}</Text>
        <Button
            title="Converter Valor"
            onPress={() => props.converterValor()}
          />
      {props.atualizado?
      <View>
        <Text style={{alignSelf: 'center',marginTop: 10}}>Informações atualizadas ({props.localDate})</Text>
      </View>:
      <View>
        <Text>Informações desatualizadas ({props.localDate})</Text>
        <TouchableOpacity onPress={()=>{
            //caso tenha informação atualizadas o botão ficará disponível para o usuario buscar a informação mais atualizada
            props.getUpdatedApiData()
          }}>
          <Text>Aperte aqui para atualizar</Text>
        </TouchableOpacity>
      </View>}
    </View>
  )

}
