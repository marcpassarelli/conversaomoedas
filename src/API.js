import AsyncStorage from '@react-native-community/async-storage';

//busca a inforamção das APIs
export function getUpdatedApiData(updateStates){
 fetch('https://api.exchangeratesapi.io/latest?base=BRL')
 .then(res => res.json())
 .then((resJson)=>{
    //chama função para salvar localmente as informações
     saveToLocal(resJson.rates,resJson.date)
     //callback para atualizar os estados em App.js
     updateStates(resJson.date,resJson.rates)
 })
 .catch((error) => {
   console.error("error: "+error);
 })
}

 //Salva as informações localmente
 async function saveToLocal(rates,date){
   try {
     await AsyncStorage.multiSet([['rates', JSON.stringify(rates)],
                             ['date', date]])
   } catch (e) {

   }
 }

//compara a data salva localmente com a data da API para verificar se as informações estão atualizadas
 export function checkIfUpdated(localDate,atualizado,naoAtualizado){
   getLastDate().then((res)=>{
     if(localDate==res){
       atualizado()
     }else{
       naoAtualizado()
     }
   })
 }

//pega a ultima data disponivel
 function getLastDate(){
   return fetch('https://api.exchangeratesapi.io/latest?base=BRL')
   .then(res => res.json())
   .then(resJson=> resJson.date)
   .catch((error) => {
     console.error("error: "+error);
   })
 }
