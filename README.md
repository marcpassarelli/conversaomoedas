# ConversaoMoedas

Desafio Android

Desenvolver uma aplicação que consome uma API REST para conversão de moedas

Utilizar uma das APIs Abaixo:
https://exchangeratesapi.io
https://ratesapi.io

Requisitos funcionais
Armazenar os dados para funcionamento offline
Deve informar se os dados apresentados são atualizados (do ultimo disponivel)
Deve poder altera livrimente entre as moedas para conversão e ver o resultado na hora
Implementar botão para realizar requisicão de atualizaçã
